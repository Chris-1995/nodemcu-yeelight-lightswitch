#include <ESP8266WiFi.h>

const char* ssid = "your ssid";
const char* password = "your wifi password";
WiFiClient espClient;
const char* host = "your bulb IP";
boolean current= true;
long millis_held=0;  

void setup_wifi() {

  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  randomSeed(micros());

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void setup() {
   Serial.begin(9600);
   setup_wifi();
   pinMode(D1, INPUT);
}
void toggleLight()
{
    if (espClient.connect(host, 55443))
    {
      Serial.println("Established Connection");
      // we are connected to the host!
      espClient.println("{\"id\":1,\"method\":\"toggle\",\"params\":[]}");
      
    }
    else
    {
      // connection failure
      Serial.println("Connection failed!");
    }  
}

void turnOffAllLamps()
{
   Serial.println("All Off");
    if (espClient.connect("Lamp IP 1", 55443))
    {
      espClient.println("{\"id\":11,\"method\":\"set_power\",\"params\":[\"off\", \"smooth\", 500]}"); 
    }
    else
    {
      Serial.println("Connection failed!");
    }
    
    if (espClient.connect("Lamp IP 2", 55443))
    {
      espClient.println("{\"id\":11,\"method\":\"set_power\",\"params\":[\"off\", \"smooth\", 500]}"); 
    }
    else
    {
      Serial.println("Connection failed!");
    }
    
    if (espClient.connect("Lamp IP 3", 55443))
    {
      espClient.println("{\"id\":12,\"method\":\"set_power\",\"params\":[\"off\", \"smooth\", 500]}"); 
    }
    else
    {
      Serial.println("Connection failed!");
    } 
}


void loop() {
  current = digitalRead(D1);

 while(current==0) {
    millis_held++;
    current=digitalRead(D1);
    delay(50);
  } 
  
  if(millis_held>0) {
    if(millis_held >= 0 && millis_held < 10)
    {
      toggleLight();  
    
    } 
    if(millis_held >= 10)
    {
      turnOffAllLamps();
    }
    millis_held = current =  0;
  }
 Serial.println(D1); 
}  
